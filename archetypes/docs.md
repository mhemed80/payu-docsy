---
title: "{{ replace .Name "_" " " | title }}"
linkTitle: "{{ replace .Name "_" " " | title }}"
date: "{{ .Date }}"
toc: "true"
---
