---
title: "Apple Pay"
date: 2019-11-05T14:13:31+01:00
draft: false
weight: 10
---

## **Introduction**

Apple Pay™ is a digital wallet which lets you make card payments in a simple and fast manner, without having to enter your card details every time. The card data is securely stored by Apple. This payment method is available for selected browsers and Apple devices (phones and computers). A full list of [**supported countries**](https://www.apple.com/ios/feature-availability/#apple-pay) and devices can be found on the Apple website.

  * [**Apple Pay iOS**](https://support.apple.com/en-us/HT208531)
  * [**Apple Pay web**](https://developer.apple.com/documentation/apple_pay_on_the_web)

## **Configuration**

Account. Next you should perform the following actions:

 * create a Merchant ID,
 * create an Apple Pay Payment Processing Certificate,
 * create an Apple Pay Merchant Identity - applies only to web integration,
 * validate the domain - applies only to web integration.

{{% alert title="If you do not wish to use Web integration, you may skip the last two steps." color="primary" %}}{{% /alert %}}

### **Creating a Merchant ID**

To create a Merchant ID, follow these steps:

  1. Log on to your Apple Developer account.
  2. Select Certificates, IDs & Profiles.
  3. Select Identifiers / Merchant IDs.
  4. Add a new Merchant ID by selecting the + sign in the top right of the screen.
  5. Enter an ID and description, and select Continue.
  6. Select Register to confirm the data entered.

### **Creating an Apple Pay Payment Processing Certificate**

{{% alert title="Contact us" color="primary" %}}
Before creating certificate please [**contact us**](https://www.payu.pl/en/help) and ask for CSR file which is required in below process
{{% /alert %}}

To create a Payment Processing Certificate identifier, follow these steps:

  1. Log on to your Apple Developer account.
  2. Select Certificates, IDs & Profiles.
  3. Select Identifiers / Merchant IDs.
  4. Select the Merchant ID that you have created, and select Edit.
  5. In the Apple Pay Payment Processing Certificate section, select Create Certificate.
  6. On the next screen select Continue.
  7. Take received CSR file from the PayU IT support department and upload it to the site, selecting Choose File and then Continue.
  8. Download the generated certificate.
  9. Send the downloaded certificate (the file apple_pay.cer) to the PayU IT support department.
