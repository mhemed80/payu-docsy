---
title: "Marketplace"
linkTitle: "Marketplace"
date: "2019-11-18T15:42:29+01:00"
toc: "true"
---

## Introduction

If you are interested in cooperation with PayU as marketplace please [contact us](https://www.payu.pl/en/help).

### Submerchant registration form

When marketplace company registration process is completed there is a special submerchant registration form available at:

```
https://secure.payu.com/boarding/#/form?lang=en&nsf=false&partnerId=MARKETPLACE_ID&marketplaceExtCustomerId=EXT_CUSTOMER_ID
```
`MARKETPLACE_ID` is unique marketplace identifier given by PayU.

`EXT_CUSTOMER_ID is unique submerchant identifier given by marketplace.

Every submerchant must be verified by PayU. If submerchant is not verified then order creation for this submerchant is not allowed and error is returned.

### Testing Marketplace
