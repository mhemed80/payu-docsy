---
title: "Classic API"
date: 2019-11-05T15:58:08+01:00
draft: false
weight: 20
toc: true
---

## **Important note**

{{% alert title="" color="primary" %}}
Documentation of Classic API is for maintenance purposes only - if you wish to integrate PayU services, please use [**REST API**](../restapi/). Classic API is no longer developed and new features and services are only available through REST API.
{{% /alert %}}
