---
title: "Restapi"
date: 2019-11-05T12:57:19+01:00
draft: false
weight: 10
description:
  Simplest and most convenient PayU integration protocol.
toc: true
---

## **Introduction**

{{< alert title="" color="primary" >}}
**Before you read this document, we advise you to go through API Overview where you can information on how to create a PayU merchant account, what integration model to choose and how to test your integration.**
{{< /alert >}}

#### **Protocol version**

This document describes integration with the **REST API 2.1** protocol. It presents various methods of implementing online payments via different PayU services and is dedicated primarily to developers wanting to implement the PayU payment services.

For information on older versions of the API  [**contact us**](https://www.payu.pl/en/help).

#### **Configuration and testing**

Click [**here**](http://static.payu.com/sites/terms/files/przewodnik_po_elementach_strony_eng.pdf) to find out how to configure Shop and POS in your PayU merchant account.

Before integrating with the PayU payment system, ensure the Point of Sale of type _**REST API (Checkout)**_ is enabled for processing payments (you can check this in the Management Panel).

Testing guidelines and test POS details (sandbox and prod) are in the **{{< target-blank "Testing" "/en/docs/overview/#testing-your-integration" >}}** section.

#### **Service description**

There are two stages to processing a payment via PayU:

  1. Customer places an order on your webpage.
  2. PayU confirms the payment has been processed successfully.

##### **Stage 1: customer places an order.**

The process is shown below:

![Payment Flow](\payment.flow.png "Payment Flow")

 1. The customer clicks on a button that is linked to the PayU payment service.
 2. The PayU system presents an order summary webpage, where the buyer confirms the payment, then redirects the buyer to a bank website.
 3. The buyer accepts the payment on the bank website. The buyer is redirected back to the PayU system.
 4. The merchant system confirms the transaction was successful and thanks the buyer.

##### **Stage 2: payment capture (optional).**

  1. The PayU system notifies the merchant system that the payment order status has changed as per its lifecycle.
  2. The merchant system confirms receipt of the notification.

{{% alert title="Current payment status is also available in the Management Panel." color="primary" %}}{{% /alert %}}

## **Creating a new order**

### **Creating a new order via the API**

{{% pageinfo %}}
For information on the supported authentication methods, see [**Signing API calls parameters**](http://developers.payu.com/en/restapi.html).
{{% /pageinfo %}}

To create a new order, use the POST method to send ```OrderCreateRequest``` to endpoint ```/api/v2_1/orders```.

{{< alert title="" color="warning" >}}
**Specify prices using the lowest currency unit** e.g. in lowest currency unit for PLN, so 1000 is equal to 10 PLN. HUF is the exception – multiply this by 100.
{{< /alert >}}

##### The following is a basic sample order:


{{< tabs tabTotal="2" tabID="1" tabName1="Production" tabName2="Sandbox" >}}
{{% tab tabNum="1" %}}

```json
curl -X POST https://secure.payu.com/api/v2_1/orders \
-H "Content-Type: application/json" \
-H "Authorization: Bearer 3e5cac39-7e38-4139-8fd6-30adc06a61bd" \
-d '{
    "notifyUrl": "https://your.eshop.com/notify",
    "customerIp": "127.0.0.1",
    "merchantPosId": "145227",
    "description": "RTV market",
    "currencyCode": "PLN",
    "totalAmount": "21000",
    "buyer": {
        "email": "john.doe@example.com",
        "phone": "654111654",
        "firstName": "John",
        "lastName": "Doe",
        "language": "pl"
    },
    "products": [
        {
            "name": "Wireless Mouse for Laptop",
            "unitPrice": "15000",
            "quantity": "1"
        },
        {
            "name": "HDMI cable",
            "unitPrice": "6000",
            "quantity": "1"
        }
    ]
}'
```

{{% /tab %}}
{{% tab tabNum="2" %}}

```json
curl -X POST https://secure.snd.payu.com/api/v2_1/orders \
-H "Content-Type: application/json" \
-H "Authorization: Bearer d9a4536e-62ba-4f60-8017-6053211d3f47" \
-d '{
    "notifyUrl": "https://your.eshop.com/notify",
    "customerIp": "127.0.0.1",
    "merchantPosId": "300746",
    "description": "RTV market",
    "currencyCode": "PLN",
    "totalAmount": "21000",
    "buyer": {
        "email": "john.doe@example.com",
        "phone": "654111654",
        "firstName": "John",
        "lastName": "Doe",
        "language": "pl"
    },
    "settings":{
        "invoiceDisabled":"true"
    },
    "products": [
        {
            "name": "Wireless Mouse for Laptop",
            "unitPrice": "15000",
            "quantity": "1"
        },
        {
            "name": "HDMI cable",
            "unitPrice": "6000",
            "quantity": "1"
        }
    ]
}'
```

{{< /tab >}}
{{< /tabs >}}


<a target="blank" href="https://payu21.docs.apiary.io/#reference/api-endpoints">
![Wypróbuj teraz](\apiary.png)
</a>

##### The following is a sample single-product order, with basic buyer data and an `extOrderId`:

```
curl -X POST https://secure.payu.com/api/v2_1/orders \
-H "Content-Type: application/json" \
-H "Authorization: Bearer 3e5cac39-7e38-4139-8fd6-30adc06a61bd" \
-d '{
    "notifyUrl": "https://your.eshop.com/notify",
    "customerIp": "127.0.0.1",
    "merchantPosId": "145227",
    "description": "RTV market",
    "currencyCode": "PLN",
    "totalAmount": "15000",
    "extOrderId":"e75247hzx0jikhern7fh3g",
    "buyer": {
        "email": "john.doe@example.com",
        "phone": "654111654",
        "firstName": "John",
        "lastName": "Doe"
    },
    "products": [
        {
            "name": "Wireless Mouse for Laptop",
            "unitPrice": "15000",
            "quantity": "1"
        }
    ]
}'
```
{{% alert title="If included, the extOrderId must be unique within each point of sale (POS)."%}}
{{% /alert %}}
Jeśli zewnętrzny identyfikator zamówienia (extOrderId) jest przesyłany w komunikacie, jego wartość musi być unikalna obrębie jednego POS-a.

After sending a request for a new order, a response will be returned by the API. The following is an example of an `OrderCreateResponse`:

```json
{
   "status":{
      "statusCode":"SUCCESS",
   },
   "redirectUri":"{payment_summary_redirection_url}",
   "orderId":"WZHF5FFDRJ140731GUEST000P01",
   "extOrderId":"{YOUR_EXT_ORDER_ID}",
}
```
The HTTP status code of the response is 302 and location header is set to `redirectUri`, which - depending on the software used - may sometimes trigger an automatic redirect.

Check [Status codes]() to learn more about possible response status codes.

The redirect process allows you to define the language of a PayU webpage by using the optional lang parameter.

To set the language of the page displayed after redirection, either use the lang parameter (see the section <buyer> for more information) or modify `redirectUri`:

```
{redirectUri z OrderCreateResponse}&lang=pl
```
The value of `sessionId` and `merchantPosId` parameters are set by the PayU system.

For more information on the available lang values refer to the section [Language versions]().

### **Integration of payment form**

{{% alert title="" color="primary" %}}
The code snippet below is an example of a simple form that enables the buyer to make a payment. Integration via form is not recommended and presented only for informational purposes for legacy implementations.
{{% /alert %}}

```html
<form method="post" action="https://secure.payu.com/api/v2_1/orders">
    <input type="hidden" name="customerIp" value="123.123.123.123">
    <input type="hidden" name="merchantPosId" value="145227">
    <input type="hidden" name="description" value="Order description">
    <input type="hidden" name="totalAmount" value="1000">
    <input type="hidden" name="currencyCode" value="PLN">
    <input type="hidden" name="products[0].name" value="Product 1">
    <input type="hidden" name="products[0].unitPrice" value="1000">
    <input type="hidden" name="products[0].quantity" value="1">
    <input type="hidden" name="notifyUrl" value="http://shop.url/notify">
    <input type="hidden" name="continueUrl" value="http://shop.url/continue">
    <input type="hidden" name="OpenPayu-Signature" value="sender=145227;algorithm=SHA-256;signature=bc94a8026d6032b5e216be112a5fb7544e66e23e68d44b4283ff495bdb3983a8">
    <button type="submit" formtarget="_blank" >Pay with PayU</button>
</form >
```

#### Customizing your form

  * You can include optional parameters, such as URLs for redirects and notifications or to set the payment page language. A full list of available parameters is available in the section .
  * Optionally enter a signature element. For more information on how to design a signature, see the section
  * For styling the form button, you can find some useful graphic resources here.

## **Notifications**
