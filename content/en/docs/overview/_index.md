---
title: "Overview"
date: 2019-11-05T12:56:11+01:00
draft: false
weight: 10
toc: true
description:
  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
---

## **Overview**

With PayU, you will quickly activate payments on your website or mobile device.

We provide a full set of endpoints which allow you to create, capture, cancel and retrieve orders, perform payouts or download reports.

To simplify the integration, you can simply use one of our plugins or our PHP SDK.

Last but not least, many shopping platforms offer an in-built integration with PayU. If this is the case, follow the instructions of how to configure payments via PayU.

### **How to start?**

For starters, you definitely need a PayU account. It may be a production PayU account (register or contact a PayU partner or sales representative) or a sandbox account (register).

After you have logged in to the panel, create a Shop and POS of REST API type.

{{% alert title="Warning" color="warning" %}}
In case you decided to use a production account to test your integration, your account will not be fully active until we verify you. However you may enable and use the test payment from the very beginning.
{{% /alert %}}

### **Integration model**

Before you start, it is important to choose the right integration model. There are several options available. The service you need to integrate and the configuration of your account may depend on the following:

{{% expand
  "Will you fullfil each order paid via PayU?" %}}
    "If no, you should disable "automatic collection" on your POS and make separate API-calls to create an order and capture or cancel it aferwards.
{{% /expand %}}

{{% expand
  "Are you selling more expensive goods or services?" %}}
    If yes, boost your sales with PayU | Installments.
{{% /expand %}}

{{% expand
  "Will you process a lot of refunds?" %}}
    If yes, you definitely should consider implementing API calls to facilitate refunds programatically, instead of performing them via the Panel.
{{% /expand %}}

{{% expand
  "Would you like to enable the payers to choose a payment method on your website?" %}}
    If yes, go for "transparent" integration.
{{% /expand %}}

{{% expand
  "Will you create user accounts for your customers?" %}}
    If yes, implement PayU | Express service and PayU will securely store card data, so your returning customers do not have to provide them each time and will be able to pay with a single click.
{{% /expand %}}

{{% expand
  "Will you charge your customers on a recurring basis (e.g. monthly)?" %}}
    If yes, use the recurring payment service.
{{% /expand %}}

{{% expand
  "Are you a billing your customers by issuing an invoice connected with a specific bank account?" %}}
    If yes, use the Mass Collect service.
{{% /expand %}}

{{% expand
  "Will you need to collect funds from customers and later distribute them to your subcontractors?" %}}
    If yes, use the Payout API to streamline funding of your business partners.
{{% /expand %}}

{{% alert title="Commercial offer" color="primary" %}}
Check our [**commercial offer**](https://www.payu.pl/en/commercial-offer) for more details.
{{% /alert %}}

### **Support**

Feel free to contact us - by fiiling the form [**here**](https://www.payu.pl/en/help).

In case you want them to check on a specific API call you made, provide the value of Correlation-Id header returned in the response from PayU.

## **Example**

#### PayU hosted payment page

Click the button – it will redirect you to a PayU hosted payment page.

<form method="post" action="https://secure.payu.com/api/v2_1/orders" target="_blank">
  <input type="hidden" name="customerIp" value="123.123.123.123">
  <input type="hidden" name="merchantPosId" value="145227">
  <input type="hidden" name="description" value="Order description">
  <input type="hidden" name="totalAmount" value="1000">
  <input type="hidden" name="currencyCode" value="PLN">
  <input type="hidden" name="products[0].name" value="Product 1">
  <input type="hidden" name="products[0].unitPrice" value="1000">
  <input type="hidden" name="products[0].quantity" value="1">
  <input type="hidden" name="notifyUrl" value="http://shop.url/notify">
  <input type="hidden" name="continueUrl" value="http://www.payu.pl">
  <input type="hidden" name="OpenPayu-Signature" value="sender=145227;algorithm=SHA-256;signature=d1c35a13e4abb71d9a7fd20298e27eb2e50f9993763c1a95c46d1e296789a942">
  <button type="submit" style="border: 0px; height: 50px; width: 313px; background: url('http://static.payu.com/pl/standard/partners/buttons/payu_account_button_long_03.png') no-repeat; cursor: pointer;"></button>
</form>

{{% alert title="" color="primary" %}}
The working example above shows the most basic integration - a payment form implementing REST API protocol. To make your integration easier to enhance in the future, consider using JSON requests.
{{% /alert %}}

#### PayU hosted widget

Click the button - it will invoke a PayU widget which you may use to securely capture card data. The widget can be also displayed inline.

To check all card payment processing options refer to the card forms section.

<form xmlns="" action="http://exampledomain.com/processOrder.php" method="post">
  <button id="pay-button" style="border: 0px; height: 50px; width: 313px; background: url('http://static.payu.com/pl/standard/partners/buttons/payu_account_button_long_03.png') no-repeat; cursor: pointer;"></button>
  </form><script xmlns="" src="https://secure.payu.com/front/widget/js/payu-bootstrap.js" pay-button="#pay-button" merchant-pos-id="145227" shop-name="Shop name" total-amount="9.99" currency-code="PLN" customer-language="en" store-card="true" customer-email="email@exampledomain.com" sig="6c9bb18db84165f53b5918380833723bc5fbb95ec5a9b73a4cb02dd60c11c64e">
</script>

## **Testing your integration**

{{% pageinfo %}}
For a basic integration, including only a redirection to PayU hosted payment page, it is perfectly enough to use the test payment method. However, if you would like to test a full set of endpoints, including e.g. refunds, consider registering for a sandbox account.
{{% /pageinfo %}}

Below is a list of test cases for your integration - check how your website handles the following:

  1. Is your user correctly redirected to PayU when PayU responds with a HTTP 302 for the POST method calls you make to /api/v2_1/orders endpoint?
  2. Do you receive and parse the notification from PayU and respond with a HTTP 200?
  3. Do you correctly establish order status during its lifecycle? Mind, order status is only provided via notifications, the statuses returned in the response from PayU apply to the request itself and to the order(!).
  4. Do you provide a continueUrl parameter? Is the customer redirected to this URL correctly after the payment process is completed?
  5. Are you prepared to handle the error message passed in the query string added to the continueUrl?
  6. In case of a PayU|Express integration - is your website ready to handle all the payment authorization scenarios?

Options 1 and 2 above can be performed either via the Panel or programatically via API.

### **Test payment method**

The test method is used for generating test payments in your PayU account. Funds from such transactions are not increasing your merchant account balance, therefore it cannot be used to test refunds. To test full functionalities of a PayU account, you may register for a sandbox account (see below).

Test transactions are disabled by default, they are also automatically blocked 3 days after being used for the last time. In order to perform the tests, activate this method of payment in **My shops** > **Shop name** > **List of POSs** > **POS name**, and change the status of a “Test payment” in the **Status** column.

{{% alert title="Remember to disable this method once you go live!" color="warning" %}}{{% /pageinfo %}}

| Value | Transaction ammount (PLN) | Time of automaticcancellation (in days) | Description |
|:------|:--------------------------|:----------------------------------------|:------------|
| t     | 0,50 - 20000,00           | 1                                       |test payment – a form is displayed where transaction status can be changed |

You may use test payment method on your own POS or use a test production POS we have already created for you:

#### Production point of sale test data

```
POS ID (pos_id):                    145227
Second key (MD5):                   13a980d4f851f3d9a1cfc792fb1f5e50
OAuth protocol - client_id:         145227
OAuth protocol - client_secret:     12f071174cb7eb79d4aac5bc2f07563f
```

### **Sandbox**

Sandbox is an almost identical copy of PayU production system. It can be used for integration and testing purposes. To use the sandbox you need a register separately in one quick step. After you are registered, you can set up own companies / shops / point of sale. Use the following links:

 * [**registration**](https://registration-merch-prod.snd.payu.com/boarding/#/registerSandbox/)
 * **{{< target-blank "login page" "https://secure.snd.payu.com/user/login?lang=en" >}}**

##### Sandbox point of sale test data

You may also use a public sandbox test POS without registering:

```
POS ID (pos_id):                 300746
Second Key (MD5):                b6ca15b0d1020e8094d9b5f8d163db54
OAuth protocol - client_id:      300746
OAuth protocol - client_secret:  2ee86a66e5d97e3fadc400c9f19b065d
```

#### Cards on sandbox.

In order to test card payments on sandbox, please use the following credentials.

| Card issuer | Number | Month | Year | CVV | 3-D Secure | Behavior |
|:------------|:-------|:------|:-----|:----|:-----------|:---------|
| Visa        | 4444333322221111 | 01 | 21 | 123 | no    | Positive authorization |
| MasterCard  |	5434021016824014 | 01 | 21 | 123 | no    | Positive authorization |
| Maestro     | 5099802211165618 | 01 | 21 | 123 | no    | Positive authorization. CVV is not required in single click payments (PayU | Express) |
| Visa        | 4012001037141112 | 01 | 21 | 123 | no    | Positive authorization |
| Maestro     | 5000105018126595 | 01 | 21 | 123 | no    | Negative authorization |
| Visa        | 4000398284360    | 01 | 21 | 123 | no    | Negative authorization |

#### Sandbox functionality.

Functionalities available on sandbox:

  * {{% expand "Pay-by-link payments:" %}}
    * mBank
    * PKO
    * Pekao
    {{% /expand %}}
  * Apple Pay,
  * BLIK OneClick,
  * Google Pay,
  * Mass Collect,
  * Recurring payments,
  * Standard transfer,
  * Installments and Pay Later,
  * Visa Checkout,
  * PayU|Mobile (Android and iOS),
  * Payouts (Panel, API),
  * Statements,
  * Refunds (Panel, API).

## **Payment methods**

{{% pageinfo %}}
Below is a full list of payment methods available from PayU.
{{% /pageinfo %}}

### **Card-based payment methods**

| Value | Transaction amount | Tiome of automatic cancellation (in days) | Description | Booking |
|:------|:-------------------|:------------------------------------------|:------------|:--------|
| c     | 0,01 - 999999,99   | 5 | Payment card (credit, debit, prepaid) - CHF, EUR, GBP, USD. | 24h/7 |
| c     | 0,05 - 999999,99   | 5 | Payment card (credit, debit, prepaid) - PLN, RON. | 24h/7 |
| c     | 0,30 - 999999,99   | 5 | Payment card (credit, debit, prepaid) - CZK. | 24h/7 |
| c     | 0,02 - 999999,99   | 5 | Payment card (credit, debit, prepaid) - BGN. | 24h/7 |
| c     | 1,00 - 999999,99   | 5 | Payment card (credit, debit, prepaid) - RUB. | 24h/7 |
| c     | 5 - 999999         | 5 | Payment card (credit, debit, prepaid) - HUF. | 24h/7 |
| c     | 0,50 - 999999,99   | 5 | Payment card (credit, debit, prepaid) - all other supported currencies. | 24h/7 |
| ap    | as above for payment cards | 5 | 	**{{< target-blank "Google Pay" "http://developers.payu.com/en/google_pay.html" >}}**   (formerly Android Pay) is a source of card data - authorization and settlement is done in the same way as for a standard card payment. In the Panel and on statements, Google Pay transactions are flagged as card payments, i.e. you may use 'ap' value to initiate Google Pay payment, but the transaction created will be flagged with 'c'. To check if the transaction was done via Google Pay, you need to check "payment flow" value. The value can be obtained through payment/get or Transaction Data Retrieve. | 24h/7 |
| ma    | as above for payment cards | 5 | **{{< target-blank "Masterpass" "http://developers.payu.com/en/masterpass.html" >}}** is a source of card data - authorization and settlement is done in the same way as for a standard card payment. In the Panel and on statements, Masterpass transactions are flagged as card payments, i.e. you may use 'ma' value to initiate Masterpass payment, but the transaction created will be flagged with 'c'. To check if the transaction was done via Masterpass, you need to check "payment flow" value. The value can be obtained through payment/get or Transaction Data Retrieve. | 24h/7 |
| vc    | as above for payment cards | 5 | 	**{{< target-blank "Visa Checkout" "http://developers.payu.com/en/visa_checkout.html" >}}** is a source of card data - authorization and settlement is done in the same way as for a standard card payment. In the Panel and on statements, Visa Checkout transactions are flagged as card payments, i.e. you may use 'vc' value to initiate Visa Checkout payment, but the transaction created will be flagged with 'c'. To check if the transaction was done via Visa Checkout, you need to check "payment flow" value. The value can be obtained through payment/get or Transaction Data Retrieve. | 24h/7 |

### **Installments and Pay later**

These payment methods are currently offered in PLN currency only.

| Value | Transaction amount (PLN) | Time of automatic cancellation (in days) | Description |
|:------|:------------------------:|:-----------------------------------------|:------------|
| ai    | 300,00 - 20000,00        | 5                                        | PayU &#124; Installments |
| dp    | 100,00 - 2000,00         | 5                                        | PayU &#124; Pay later |

### **Polish pay-by-link online transfers**

| Value | Transaction amount (PLN) | Time of automatic cancellation (in days) | Description | Booking |
|:------|:------------------------:|:-----------------------------------------|:------------|:--------|
| blik  | 0,01 - 999999,99         | 10                | BLIK        | 24h/7 |
| mt    | 0,37 - 999999,99         | 10                | mTransfer - mBank | 24h/7 |
| mtex  | 0,50 - 999999,99         | 10                | mTransfer mobilny - mBank(*) | 24h/7 |
| wb    | 0,37 - 7000,00	         | 10                | Przelew24 - Santander (form. BZ WBK) | 24h/7 |
| pe    |	0,37 - 999999,99         | 10                | Pekao24Przelew - Bank Pekao | 24h/7 |
| ip    |	0,37 - 999999,99         | 10                | Płacę z Inteligo | 24h/7 |
| ipp   |	0,37 - 999999,99         | 10                | Płać z iPKO | 24h/7 |

### **Chech pay-by-link online transfer payment methods**
<!-- Hyperlinks -->
