---
title: "Wprowadzenie"
date: 2019-11-05T10:31:22+01:00
draft: false
weight: 10
toc: true
---

## **Wprowadzenie**

PayU umożliwia łatwą akceptację płatności na Twojej stronie lub urzadzeniu mobilnym.

Oferujemy pełen zestaw API, które pozwalają tworzyć, rozliczać, anulować i pobierać zamówienia, wykonywać wypłaty i pobierać raporty.

W celu uproszczenia integracji, można użyć jednej z naszych wtyczek lub naszego PHP SDK.

Ponadto, wiele platform sklepowych oferuje wbudowaną integrację z PayU. Jeśli platforma jakiej używasz do nich należy, postępuj zgodnie z podanymi w niej instrukcjami aby szybko skonfigurować płatność przez PayU.

### **Jak zacząć?**

Na początek należy założyć konto PayU dla sprzedawców (obiorców płatności). Może to być konto produkcyjne (rejestruj lub skontaktuj się z partnerem PayU lub przedstawicielem handlowym) lub konto testowe na sandboksie (rejestruj).

Po zalogowaniu się do panelu administracyjnego konta PayU, stwórz Sklep i Punkt Płatności w typie REST API.

Uwaga: jeżli zacząłeś od razu od konta produkcyjnego, nie będzie ono w pełni aktywne, dopóki nie zostanie przez nas zakończona wymagana prawem weryfikacja. Jednakże płatność testowa dostępna jest dla Ciebie już od samego początku.

### **Model integracji**
