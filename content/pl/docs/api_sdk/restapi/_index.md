---
title: "Rest API"
date: 2019-11-05T10:33:40+01:00
draft: false
weight: 10
toc: true
---

## **Wprowadzenie**

{{% alert title="Notka" color="primary" %}}
Zanim zaczniesz zapoznawać się z poniższym dokumentem, warto przeczytać wprowadzenie, gdzie znajdują się informacje o tym jak zacząć współpracę z PayU i integrację API, jaki model integracji wybrać i jak ją testować.
{{% /alert %}}

#### **Informacje o aktualnej wersji**

Dokument zawiera informacje o protokole **REST API w wersji 2.1**.

Jeżeli poszukujesz informacji o starszej wersji API [skontaktuj się z nami](https://www.payu.pl/pomoc).

#### **Informacje podstawowe**

Tutaj znajdziesz informacje jak skonfigurować sklep i POS po zalogowaniu się do konta PayU dla sprzedających.

Przed rozpoczęciem procesu integracji należy się upewnić, że punkt płatności na którym będą realizowane zamówienia jest typu **REST API (Checkout)** (do sprawdzenia na ekranie sklepów w Panelu Managera).

Informacje nt. testowania i dane testowych POSów (produkcja i sandbox) znajdują się w sekcji Testowanie.

#### **Opis usługi**

Dokumentacja ma na celu przedstawienie sposobu wdrożenia płatności internetowych w sklepie internetowym za pośrednictwem usług PayU. Dokumentacja podzielona jest na kilka części. Pierwsza to wprowadzenie w usługę płatności. Następna to minimalny przykład wdrożenia płatności w sklepie internetowym w tzw. trybie hosted (przekierowanie na stronę PayU). Kolejne dwie części poświęcone są na omówienie procesów powiadomienia oraz zwrotów. Kolejne części to przykłady oraz referencje.

Materiał ten jest przede wszystkim przeznaczony dla deweloperów, którzy chcą zintegrować usługę płatności PayU ze swoim sklepem internetowym.

Proces obsługi płatności w sklepie internetowym za pośrednictwem usługi PayU składa się z dwóch etapów.

  1. Złożenie zamówienia przez kupującego na stronie sklepu.
  2. Potwierdzenia prawidłowego rozliczenia płatności zrealizowanej przez usługę PayU.

##### **Etap pierwszy. Złożenie zamówienia przez kupującego.**

Proces jest przedstawiony na poniższym diagramie.

![Payment Flow](\payment.flow.png "Payment Flow")

 1. **Kupujący** klika w przycisk reprezentujący usługę płatności PayU.
 2. **System PayU** prezentuje stronę z podsumowaniem zamówienia na której **kupujący** potwierdza płatność. **System PayU** przekierowuje kupującego na stronę banku.
 3. **Kupujący** akceptuje płatność na stronie Banku. Nastepuje przekierowanie kupującego ponownie na stronę systemu PayU.
 4. **System Sprzedawcy** prezentuje podziękowanie za transakcje.

##### **Etap drugi (opcjonalny). Rozliczenie (odebranie) płatności.**

  1. **System PayU** powiadamia system sprzedawcy o zmianie statusu procesowanej płatności zgodnie z jej cyklem za pomocą powiadomień ("notyfikacji").
  2. **System sprzedawcy** potwierdza odebranie powiadomienia.

{{% alert title="Notka" color="primary" %}}
Aktualny status danej płatności możesz obejrzeć w panelu menadżera.
{{% /alert %}}

## **Tworzenie nowego zamówienia**

### **Tworzenie nowego zamówienia przez API**

Metody uwierzytelnienia znajdują się w: Uwierzytelnienie użytkownika API.

W celu utworzenia nowego zamówienia należy za pomocą metody POST wyśłać komunikat OrderCreateRequest na endpoint /api/v2_1/orders.

{{% alert title="Uwaga" color="warning" %}}
**Wszystkie kwoty należy podawać w najmniejszej jednostce dla danej waluty**, np. w groszach dla PLN czyli "1000" oznacza 10 zł. Wyjątkiem jest HUF, który należy pomnożyć razy 100.
{{% /alert %}}

Przykład zamówienia z podstawowymi danymi:

```json
curl -X POST https://secure.payu.com/api/v2_1/orders \
-H "Content-Type: application/json" \
-H "Authorization: Bearer 3e5cac39-7e38-4139-8fd6-30adc06a61bd" \
-d '{
    "notifyUrl": "https://your.eshop.com/notify",
    "customerIp": "127.0.0.1",
    "merchantPosId": "145227",
    "description": "RTV market",
    "currencyCode": "PLN",
    "totalAmount": "21000",
    "buyer": {
        "email": "john.doe@example.com",
        "phone": "654111654",
        "firstName": "John",
        "lastName": "Doe",
        "language": "pl"
    },
    "products": [
        {
            "name": "Wireless Mouse for Laptop",
            "unitPrice": "15000",
            "quantity": "1"
        },
        {
            "name": "HDMI cable",
            "unitPrice": "6000",
            "quantity": "1"
        }
    ]
}'
```
<a target="blank" href="https://payu21.docs.apiary.io/#reference/api-endpoints">
![Wypróbuj teraz](\apiary.png)
</a>

Przykład zamówienia z jednym produktem i podstawowymi danymi kupującego oraz extOrderId:

```json
curl -X POST https://secure.payu.com/api/v2_1/orders \
-H "Content-Type: application/json" \
-H "Authorization: Bearer 3e5cac39-7e38-4139-8fd6-30adc06a61bd" \
-d '{
    "notifyUrl": "https://your.eshop.com/notify",
    "customerIp": "127.0.0.1",
    "merchantPosId": "145227",
    "description": "RTV market",
    "currencyCode": "PLN",
    "totalAmount": "15000",
    "extOrderId":"e75247hzx0jikhern7fh3g",
    "buyer": {
        "email": "john.doe@example.com",
        "phone": "654111654",
        "firstName": "John",
        "lastName": "Doe"
    },
    "products": [
        {
            "name": "Wireless Mouse for Laptop",
            "unitPrice": "15000",
            "quantity": "1"
        }
    ]
}'
```
Jeśli zewnętrzny identyfikator zamówienia (extOrderId) jest przesyłany w komunikacie, jego wartość musi być unikalna obrębie jednego POS-a.

Po wysłaniu żądania utworzenia nowego zamówienia zostanie odesłana odpowiedź. Pole redirectUri zawiera URL pod jaki należy przekierować kupującego.

Przykład odpowiedzi na żądanie utworzenia nowego zamówienia:
```json
{
   "status":{
      "statusCode":"SUCCESS",
   },
   "redirectUri":"{url_do_przekierowania_na_stronę_podsumowania_płatności}",
   "orderId":"WZHF5FFDRJ140731GUEST000P01",
   "extOrderId":"{twój_identyfikator_zamówienia}",
}
```
{{% alert title="Notka" color="primary" %}}
Odpowiedź zwraca kod HTTP 302 oraz nagłówek Location ustawiony na redirectUri. Może być to powodem automatycznych przekierowań.
{{% /alert %}}

Więcej informacji o możliwych kodach odpowiedzi znajduje się w sekcji Kody statusów.

W trakcie wykonywania przekierowania możliwe jest ustawienie języka strony PayU przez użycie opcjonalego parametru lang.

Aby ustawić język strony, na którą zostanie przeniesiony użytkownik po przekierowaniu, należy użyć parametru language w sekcji <buyer> albo przekształcić redirectUri poprzez dokonanie następującego złożenia:

```
{redirectUri z OrderCreateResponse}&lang=pl
```
Akceptowane wartości parametru lang są tutaj.

### **Integracja formularza zamówienia**

{{% alert title="Notka" color="primary" %}}
Sekcja zawiera minimalny przykład formularza umożliwiający kupującemu dokonanie płatności. **Integracja poprzez formularz nie jest zalecana** i prezentowana jedynie w celach informacyjnych dla istniejących implementacji.
{{% /alert %}}


<!-- Hyperlinks -->
