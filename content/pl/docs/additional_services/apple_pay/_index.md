---
title: "Apple Pay"
date: 2019-11-05T12:41:47+01:00
draft: false
weight: 10
---

## **Wstęp**

Apple Pay™ to cyfrowy portfel, który w prosty i szybki sposób pozwala płacić kartą, bez każdorazowego uzupełniania jej danych. Dane karty są bezpiecznie przechowywane przez firmę Apple. Ta metoda płatności jest dostępna dla wybranych przeglądarek oraz urządzeń (telefonów i komputerów) firmy Apple. Pełną listę wspieranych krajów oraz urządzeń znajdziesz na stronach Apple.

 * [Apple Pay iOS](https://support.apple.com/en-us/HT208531)
 * [Apple Pay web](https://developer.apple.com/documentation/apple_pay_on_the_web)

{{% alert title="Uwaga" color="warning" %}}
Poniższy opis dotyczy udostępnienia tej usługi w aplikacji mobilnej lub poprzez wywołanie aplikacji Apple Pay bezpośrednio ze strony odbiorcy płatności (sklepu internetowego).
{{% /alert %}}

## **Konfiguracja**

Aby obsługiwać płatności Apple Pay w swojej aplikacji mobilnej lub na stronie internetowej w pierwszej kolejności potrzebujesz [Apple Development](https://developer.apple.com/) Account. Następnie musisz wykonać następujące czynności:

  * utworzyć identyfikator Merchant ID,
  * utworzyć Apple Pay Payment Processing Certificate,
  * utworzyć Apple Pay Merchant Identity – dotyczy tylko integracji web,
  * zwalidować domenę – dotyczy tylko integracji web.

{{% alert title="Jeżeli nie chcesz korzystać z integracji web, możesz pominąć dwa ostatnie etapy." color="primary" %}}
{{% /alert %}}

### **Utworzenie identyfikatora Merchant ID**

W celu utworzenia identyfikatora Merchant ID wykonaj kolejno następujące kroki:

  1. Zaloguj się do swojego konta [Apple Developer](https://developer.apple.com/).
  2. Wybierz opcję Certificates, IDs & Profiles.
  3. Wybierz opcję Identifiers / Merchant IDs.
  4. Dodaj nowy Merchant ID, wybierając znak + w prawej górnej części ekranu.
  5. Wprowadź ID oraz opis i wybierz opcję Continue.
  6. Potwierdź wprowadzone dane, wybierając opcję Register.
